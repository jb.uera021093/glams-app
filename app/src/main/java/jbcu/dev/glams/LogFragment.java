package jbcu.dev.glams;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class LogFragment extends ListFragment implements OnItemClickListener {
    ProgressDialog pDialog;
    private static final String TAG_ID = "id";
    private static final String TAG_LOG_NAME = "name";
    private SQLiteHandler db;
    ArrayList<HashMap<String, String>> logList;
    private static final String TAG = Home.class.getSimpleName();

    public LogFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState)  {

        View rootView = inflater.inflate(R.layout.fragment_log, null, false);
        logList = new ArrayList<HashMap<String, String>>();

        db = new SQLiteHandler(getActivity().getApplicationContext());


        getLogs();
        return rootView;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getListView().setOnItemClickListener(this);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

       // Log.d("test",id+"");
        TextView txt_pid= ((TextView) view.findViewById(R.id.pid));
        String workoutId =txt_pid.getText().toString();
        Toast.makeText(getActivity(),"id: "+workoutId,
                Toast.LENGTH_LONG).show();


        Intent intent = new Intent(getActivity(), LogActivity.class);
        intent.putExtra("id",workoutId);
        startActivity(intent);
    }

    private void getLogs() {
        String tag_string_req = "get_log";
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading LogActivity List ...");
        showDialog();
        User user = db.getUser();
        StringRequest strReq = new StringRequest(Request.Method.GET, AppConfig.URL_LOGS+"/"+user.getIdNumber()+"/status/0", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Agents Response: " + response.toString());
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String result = jObj.getString("message");
                    if (result.equals("success")) {

                        JSONObject data = jObj.getJSONObject("data");
                        JSONArray logs = data.getJSONArray("log");
                        int n = logs.length();

                        for(int i = 0; i < n; i++){

                            JSONObject c= logs.getJSONObject(i);
                            String id = c.getString("ID");
                            String name = "Workout: "+ c.getString("DateNow");
                            HashMap<String, String> map = new HashMap<String, String>();

                            map.put(TAG_ID, id);
                            map.put(TAG_LOG_NAME, name);
                            logList.add(map);
                        }


                        initializeList();


                    } else {
                        String errorMsg = jObj.getString("message");
                        Toast.makeText(getActivity().getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(), "Login Failed!", Toast.LENGTH_LONG).show();
                hideDialog();
            }


        }) {


        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    public void initializeList() {



        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                ListAdapter adapter = new SimpleAdapter(
                        getActivity(), logList,
                        R.layout.list_log,
                        new String[]{TAG_ID, TAG_LOG_NAME, },
                        new    int[]{R.id.pid, R.id.logName});
                setListAdapter(adapter);
            }

        });
    }
    private void hideDialog() {
        if (
                pDialog.isShowing())
            pDialog.dismiss();
    }
    private void showDialog() {
        if (!pDialog.isShowing()) pDialog.show();
    }

}
