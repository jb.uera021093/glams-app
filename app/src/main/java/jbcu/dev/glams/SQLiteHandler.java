package jbcu.dev.glams;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by gd on 17/12/2016.
 */

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // Database Name
    private static final String DATABASE_NAME = "glams";
    //Database Version
    private static final int DATABASE_VERSION = 1;

    //for User Table
    private static final String TABLE_USER = "user";
    private static final String KEY_ID = "id";
    private static final String KEY_ID_NUMBER = "idNumber";
    private static final String KEY_FIRSTNAME = "firstName";
    private static final String KEY_LASTNAME = "lastName";
    private static final String KEY_MI = "middleInitial";
    private static final String KEY_BIRTHDAY = "birthDay";
    private static final String KEY_CONTACT_NUMBER = "contactNumber";
    private static final String KEY_GENDER = "gender";
    private static final String KEY_NATIONALITY = "nationality";
    private static final String KEY_CIVILSTATUS = "civil_status";
    private static final String KEY_CREDENTIAL = "password";



    //Constants
    private static final String CREATE_TABLE = "CREATE TABLE ";
    private static final String PRIMARY_KEY = " INTEGER PRIMARY KEY,";
    private static final String CONSTANT_TEXT = " TEXT,";
    private static final String CONSTANT_TEXT_TERMINATOR = " TEXT)";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.d(TAG, "CREATION OF USER : = = = = = = = =  USER  tables created");
            String createUserTable =
                    CREATE_TABLE + TABLE_USER + "("
                    + KEY_ID + PRIMARY_KEY
                    + KEY_ID_NUMBER + CONSTANT_TEXT
                    + KEY_FIRSTNAME + CONSTANT_TEXT
                    + KEY_MI + CONSTANT_TEXT
                    + KEY_LASTNAME + CONSTANT_TEXT
                    + KEY_BIRTHDAY + CONSTANT_TEXT
                    + KEY_CONTACT_NUMBER + CONSTANT_TEXT
                    + KEY_GENDER + CONSTANT_TEXT
                    + KEY_NATIONALITY + CONSTANT_TEXT
                    + KEY_CIVILSTATUS + CONSTANT_TEXT
                    + KEY_CREDENTIAL + CONSTANT_TEXT_TERMINATOR;
            db.execSQL(createUserTable);
        } catch (SQLiteException e) {
            Log.d(TAG, "Error Creating Table User:" + e);

        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //on upgrade do something
        try{


            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
            Log.d(TAG, "Database tables deleted");
            onCreate(db);

        }
        catch (Exception e){
            Log.d("ERROR: = = = = = = =", e.getMessage());
        }

    }

    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FIRSTNAME, user.getFirstName()); // Name
        values.put(KEY_LASTNAME, user.getLastName()); // Name
        values.put(KEY_MI, user.getMiddleInitial()); // Name
        values.put(KEY_ID_NUMBER, user.getIdNumber()); // Name
        values.put(KEY_BIRTHDAY, user.getBirthDay()); // Name
        values.put(KEY_CONTACT_NUMBER, user.getContactNumber()); // Name
        values.put(KEY_GENDER, user.getGender()); // Name
        values.put(KEY_NATIONALITY, user.getNationality()); // Email
        values.put(KEY_CIVILSTATUS, user.getCivilStatus()); // Email
        values.put(KEY_CREDENTIAL, user.getPassword()); // Created At

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection
        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    public User getUser() {
        User user  = new User();
        String selectQuery = "SELECT  * FROM " + TABLE_USER+";";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.d("size in cursor",cursor.getCount()+"");
        if (cursor.moveToFirst()) {
            user.setIdNumber(cursor.getString(1));
            user.setFirstName(cursor.getString(2));
            user.setMiddleInitial(cursor.getString(3));
            user.setLastName(cursor.getString(4));
            user.setBirthDay(cursor.getString(5));
            user.setContactNumber(cursor.getString(6));
            user.setGender(cursor.getString(7));
            user.setNationality(cursor.getString(8));
            user.setCivilStatus(cursor.getString(9));
            user.setPassword(cursor.getString(10));
        }
        cursor.close();
        db.close();
        Log.d(TAG, "Fetching symptoms from Sqlite: " + user.toString());
        return user;
    }

    public void deleteUser(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_USER, null, null);

    }

}
