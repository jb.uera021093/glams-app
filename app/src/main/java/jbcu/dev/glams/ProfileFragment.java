package jbcu.dev.glams;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class ProfileFragment extends Fragment {


    TextView txt_name,txt_Nationality,txt_civilStatus,txt_gender,txt_birthDay,txt_contactNumber,txt_idNumber;
    private SQLiteHandler db;
    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_profile, null, false);
        db = new SQLiteHandler(getActivity().getApplicationContext());
        txt_name = (TextView)rootView.findViewById(R.id.txt_name);
        txt_Nationality = (TextView)rootView.findViewById(R.id.txt_Nationality);
        txt_civilStatus = (TextView)rootView.findViewById(R.id.txt_civilStatus);
        txt_gender = (TextView)rootView.findViewById(R.id.txt_gender);
        txt_birthDay = (TextView)rootView.findViewById(R.id.txt_birthDay);
        txt_contactNumber = (TextView)rootView.findViewById(R.id.txt_contactNumber);
        txt_idNumber = (TextView)rootView.findViewById(R.id.txt_idNumber);

        User user = db.getUser();

        txt_name.setText(user.getFirstName()+" "+user.getLastName());
        txt_Nationality.setText(user.getNationality());
        txt_civilStatus.setText(user.getCivilStatus());
        txt_gender.setText(user.getGender());
        txt_birthDay.setText(user.getBirthDay());
        txt_contactNumber.setText(user.getContactNumber());
        txt_idNumber.setText(user.getIdNumber());


        return rootView;
    }

}
