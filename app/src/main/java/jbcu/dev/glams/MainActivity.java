package jbcu.dev.glams;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
EditText edit_username, edit_password;
    Button btn_signin;
    ProgressDialog pDialog;
    private SQLiteHandler db;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        edit_username = (EditText)findViewById(R.id.edit_username);
        edit_password = (EditText)findViewById(R.id.edit_password);
        btn_signin =(Button)findViewById(R.id.btn_signin);

        db = new SQLiteHandler(getApplicationContext());


        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = edit_username.getText().toString();
                String password = edit_password.getText().toString();
                db.deleteUser();

                signIn(username,password);
            }
        });
    }

    public void signIn(String username, String password){

        if(username.equals("")){
            Toast.makeText(MainActivity.this, "Please enter your username!",
                    Toast.LENGTH_LONG).show();
        }
        if(password.equals("")){
            Toast.makeText(MainActivity.this, "Please enter your password!",
                    Toast.LENGTH_LONG).show();
        }

        if(!password.equals("")&&!username.equals("")){
            checkLogin(username,password);
        }

    }

    private void checkLogin(final String username, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                // LogActivity.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);

                    String result = jObj.getString("message");
                    if(result.equals("success"))
                    {
                        JSONObject data = jObj.getJSONObject("data");
                        JSONObject profile = data.getJSONObject("profile");
                        User user = new User();
                        user.setFirstName(profile.getString("FirstName"));
                        user.setLastName(profile.getString("LastName"));
                        user.setMiddleInitial(profile.getString("MI"));
                        user.setContactNumber(profile.getString("ContactPrefix")+profile.getString("ContactNumber"));
                        user.setCivilStatus(profile.getString("CivilStatus"));
                        user.setGender(profile.getString("Gender"));
                        user.setBirthDay(profile.getString("Birthday"));
                        user.setNationality(profile.getString("Nationality"));
                        user.setPassword(password);
                        user.setIdNumber(username);

                        db.addUser(user);
                        Intent intent = new Intent(MainActivity.this,Home.class);
                        startActivity(intent);
                        finish();
                        Toast.makeText(MainActivity.this, "You have successfully logged in!",
                                Toast.LENGTH_LONG).show();

                    }

                    if(result.equals("failed"))
                    {
                        Toast.makeText(MainActivity.this, "Please enter the correct username and password!",
                                Toast.LENGTH_LONG).show();

                    }




                  /*  JSONObject data = jObj.getJSONObject("data");
                    String id = data.getString("id");*/



                } catch (JSONException e) {
                    // JSON error
                    hideDialog();
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Login Failed! \n\n Please check your username and password... ", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getApplicationContext(),
                            "Network is Unreachable! \n Connection timeout...", Toast.LENGTH_LONG).show();

                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(),
                            "Login Failed! \n\n Please check your username and password... ", Toast.LENGTH_LONG).show();

                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),
                            "Network is Unreachable! \n Please Check your Internet connection...", Toast.LENGTH_LONG).show();

                } else if (error instanceof NetworkError) {
                } else if (error instanceof ParseError) {
                }
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_number", username);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
