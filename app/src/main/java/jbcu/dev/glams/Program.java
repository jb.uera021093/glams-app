package jbcu.dev.glams;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Program extends AppCompatActivity {
    Intent intent;
    String workoutName;
    ProgressDialog pDialog;
    private SQLiteHandler db;
    TextView txt_programName;
    LinearLayout ll_workout;
    private static final String TAG = Program.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program);
        intent =getIntent();
        workoutName = intent.getStringExtra("workout");

        txt_programName = (TextView)findViewById(R.id.txt_programName);
        ll_workout = (LinearLayout) findViewById(R.id.ll_workout);
        getProgram(workoutName);
    }
    private void getProgram(String workout) {
        String tag_string_req = "get_log";
        pDialog = new ProgressDialog(Program.this);
        pDialog.setMessage("Loading LogActivity List ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET, AppConfig.URL_PROGRAM+workout, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Agents Response: " + response.toString());
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String result = jObj.getString("message");
                    if (result.equals("success")) {

                        JSONObject data = jObj.getJSONObject("data");
                        txt_programName.setText(data.getString("Program"));
                        JSONArray workout = data.getJSONArray("workout");
                        int n = workout.length();

                        for(int i = 0; i < n; i++){

                            JSONObject c= workout.getJSONObject(i);
                            final TextView txtWorkout = new TextView(Program.this);
                            txtWorkout.setText(c.getString("WorkOut"));
                            txtWorkout.setId(Integer.parseInt(c.getString("ID")));
                            txtWorkout.setTextSize(12);
                            txtWorkout.setHint(c.getString("WorkOut"));
                            txtWorkout.setAllCaps(true);

                           txtWorkout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(Program.this,Workout.class);
                                    Log.d("workout name: ", txtWorkout.getText().toString());
                                    intent.putExtra("name",txtWorkout.getText().toString());
                                    startActivity(intent);
                                }
                            });
                            ll_workout.addView(txtWorkout);

                        }

                    } else {
                        String errorMsg = jObj.getString("message");
                        Toast.makeText(Program.this, errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Program.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(Program.this, "Login Failed!", Toast.LENGTH_LONG).show();
                hideDialog();
            }


        }) {


        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void hideDialog() {
        if (
                pDialog.isShowing())
            pDialog.dismiss();
    }
    private void showDialog() {
        if (!pDialog.isShowing()) pDialog.show();
    }

}

