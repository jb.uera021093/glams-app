package jbcu.dev.glams;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LogActivity extends AppCompatActivity {
    Intent intent;
    String logId;
    ProgressDialog pDialog;
    private SQLiteHandler db;
    private static final String TAG = LogActivity.class.getSimpleName();
    TextView txt_programPlan,txt_timeIn,txt_workout1,txt_workout2,txt_workout3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        txt_programPlan = (TextView)findViewById(R.id.txt_programPlan);
        txt_timeIn = (TextView)findViewById(R.id.txt_timeIn);
        txt_workout1 = (TextView)findViewById(R.id.txt_workout1);
        txt_workout2 = (TextView)findViewById(R.id.txt_workout2);
        txt_workout3 = (TextView)findViewById(R.id.txt_workout3);
        db = new SQLiteHandler(getApplicationContext());

        intent =getIntent();
        logId = intent.getStringExtra("id");
        User user = db.getUser();

        getLog(user.getIdNumber(),logId);

        txt_workout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LogActivity.this,Program.class);
                intent.putExtra("workout",txt_workout1.getText().toString());
                startActivity(intent);
                finish();
            }
        });

        txt_workout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LogActivity.this,Program.class);
                intent.putExtra("workout",txt_workout2.getText().toString());
                startActivity(intent);
                finish();
            }
        });

        txt_workout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LogActivity.this,Program.class);
                intent.putExtra("workout",txt_workout3.getText().toString());
                startActivity(intent);
                finish();
            }
        });


    }

    private void hideDialog() {
        if (
                pDialog.isShowing())
            pDialog.dismiss();
    }
    private void showDialog() {
        if (!pDialog.isShowing()) pDialog.show();
    }

    private void getLog(String userId,String workoutId) {
        String tag_string_req = "get_log";
        pDialog = new ProgressDialog(LogActivity.this);
        pDialog.setMessage("Loading LogActivity List ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET, AppConfig.URL_LOGS+"/"+userId+"/"+workoutId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Agents Response: " + response.toString());
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String result = jObj.getString("message");
                    if (result.equals("success")) {

                        JSONObject data = jObj.getJSONObject("data");

                        txt_programPlan.setText(data.getString("ProgramPlan"));
                        txt_timeIn.setText(data.getString("TimeIn"));
                        txt_workout1.setText(data.getString("WorkoutPlan1"));
                        txt_workout2.setText(data.getString("WorkoutPlan2"));
                        txt_workout3.setText(data.getString("WorkoutPlan3"));


                    } else {
                        String errorMsg = jObj.getString("message");
                        Toast.makeText(LogActivity.this, errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LogActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(LogActivity.this, "Login Failed!", Toast.LENGTH_LONG).show();
                hideDialog();
            }


        }) {


        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

}
