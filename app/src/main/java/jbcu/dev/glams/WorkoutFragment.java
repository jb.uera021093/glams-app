package jbcu.dev.glams;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class WorkoutFragment extends ListFragment implements AdapterView.OnItemClickListener {
    ProgressDialog pDialog;
    private static final String TAG_ID = "id";
    private static final String TAG_WORKOUT_NAME = "name";
    private SQLiteHandler db;
    ArrayList<HashMap<String, String>> workoutList;
    private static final String TAG = Home.class.getSimpleName();

    public WorkoutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_workout, null, false);
        workoutList = new ArrayList<HashMap<String, String>>();

        db = new SQLiteHandler(getActivity().getApplicationContext());


        getWorkouts();
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    private void getWorkouts() {
        String tag_string_req = "get_workout";
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading Workout List ...");
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.GET, AppConfig.URL_WORKOUT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "workout Response: " + response.toString());
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    String result = jObj.getString("message");
                    if (result.equals("success")) {

                        JSONObject data = jObj.getJSONObject("data");
                        JSONArray workouts = data.getJSONArray("workout");
                        int n = workouts.length();
                        Log.d("workout total",n+"");

                        for(int i = 0; i < n; i++){

                            JSONObject c= workouts.getJSONObject(i);

                            String id = c.getString("ID");
                            String name = c.getString("WorkOut");
                            HashMap<String, String> map = new HashMap<String, String>();

                            map.put(TAG_ID, id);
                            map.put(TAG_WORKOUT_NAME, name);
                            workoutList.add(map);
                        }
                        

                        initializeList();


                    } else {
                        String errorMsg = jObj.getString("message");
                        Toast.makeText(getActivity().getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }


        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(), "Login Failed!", Toast.LENGTH_LONG).show();
                hideDialog();
            }


        }) {


        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    public void initializeList() {



        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                ListAdapter adapter = new SimpleAdapter(
                        getActivity(), workoutList,
                        R.layout.list_workout,
                        new String[]{TAG_ID, TAG_WORKOUT_NAME, },
                        new    int[]{R.id.pid, R.id.workoutName});
                setListAdapter(adapter);
            }

        });
    }
    private void hideDialog() {
        if (
                pDialog.isShowing())
            pDialog.dismiss();
    }
    private void showDialog() {
        if (!pDialog.isShowing()) pDialog.show();
    }

}
