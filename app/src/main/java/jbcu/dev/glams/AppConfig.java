package jbcu.dev.glams;

/**
 * Created by jb021093 on 19/07/2016.
 */
public class AppConfig {
    public static String IP_ADDRESS = "http://192.168.43.14:9000";
    public static String URL_LOGIN = IP_ADDRESS+"/api/v1/user/auth";   //Login
    public static String URL_WORKOUT = IP_ADDRESS+"/api/v1/workouts";   // workout list
    public static String URL_LOGS = IP_ADDRESS+"/api/v1/logs";   // workout list
    public static String URL_PROGRAM = IP_ADDRESS+"/api/v1/program/";   // program list
    public static String URL_COMPLETE = IP_ADDRESS+"/api/v1/complete";   // program list
}
