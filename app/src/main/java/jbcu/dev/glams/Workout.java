package jbcu.dev.glams;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Workout extends AppCompatActivity {
ImageView image1,image2;
TextView workout_name;
Button button_start, button_end;
    Date start_date, end_date;
    ProgressDialog pDialog;
    private SQLiteHandler db;
    private static final String TAG = Workout.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        db = new SQLiteHandler(getApplicationContext());

        workout_name = (TextView)findViewById(R.id.workout_name);
        image1 = (ImageView) findViewById(R.id.image1);
        image2 = (ImageView)findViewById(R.id.image2);
        button_start = (Button) findViewById(R.id.button_start);
        button_end = (Button) findViewById(R.id.button_end);
        Intent intent = getIntent();
        final String name = intent.getStringExtra("name");
        workout_name.setText(name);
        String cleanName = name.replaceAll("\\s","_");
        Picasso.with(this).load(AppConfig.IP_ADDRESS+"/images/"+cleanName+".jpg").into(image1);
        Picasso.with(this).load(AppConfig.IP_ADDRESS+"/images/"+cleanName+"1.jpg").into(image2);


        button_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_date = new Date();
                button_start.setVisibility(View.GONE);
                button_end.setVisibility(View.VISIBLE);
            }
        });

        button_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                end_date = new Date();

                completeWorkout(start_date,end_date,name,db.getUser().getIdNumber());

            }
        });
    }

    private void completeWorkout(final Date start_date, final Date end_date, final String workout, final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Submitting Workout ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_COMPLETE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                // LogActivity.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);

                    String result = jObj.getString("message");
                    if(result.equals("success"))
                    {
                        Intent intent = new Intent(Workout.this,Home.class);
                        startActivity(intent);
                        finish();
                        Toast.makeText(Workout.this, "You have successfully Completed your workout!",
                                Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // JSON error
                    hideDialog();
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.toString());

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getApplicationContext(),
                            "Network is Unreachable! \n Connection timeout...", Toast.LENGTH_LONG).show();

                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(),
                            "Login Failed! \n\n Please check your username and password... ", Toast.LENGTH_LONG).show();

                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),
                            "Network is Unreachable! \n Please Check your Internet connection...", Toast.LENGTH_LONG).show();

                } else if (error instanceof NetworkError) {
                } else if (error instanceof ParseError) {
                }
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_num", username);
                params.put("start_time", start_date.toString());
                params.put("end_time", end_date.toString());
                params.put("workout", workout);
                params.put("date", new Date().toString());
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
